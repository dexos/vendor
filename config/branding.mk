BUILD_DATE := $(shell date +%Y%m%d)
BUILD_TIME := $(shell date +%H%M)
# Versioning System
# Use signing keys for only official
ifeq ($(DELUXE_BUILD_TYPE),OFFICIAL)
    PRODUCT_DEFAULT_DEV_CERTIFICATE := ./.keys/releasekey
endif

ifndef DELUXE_BUILD_TYPE
    DELUXE_BUILD_TYPE := UNOFFICIAL
endif

# Set all versions
DELUXE_BASE_VERSION := v4.0
DELUXE_PLATFORM_VERSION := 11
DELUXE_VERSION := $(TARGET_PRODUCT)-$(BUILD_DATE)-$(BUILD_TIME)-$(DELUXE_PLATFORM_VERSION)-$(DELUXE_BASE_VERSION)-$(DELUXE_BUILD_TYPE)
ROM_FINGERPRINT := DeluxeOS/$(PLATFORM_VERSION)/$(DELUXE_BUILD_TYPE)/$(BUILD_DATE)$(BUILD_TIME)

# Declare it's a deluxe build
STATIX_BUILD := true
dexos-rom :=true
