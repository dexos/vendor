#
# Copyright (C) 2018 The deluxeOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Pixel2 sounds
PRODUCT_COPY_FILES += \
	vendor/deluxe/prebuilt/common/media/audio/alarms/A_real_hoot.ogg:system/media/audio/alarms/A_real_hoot.ogg \
	vendor/deluxe/prebuilt/common/media/audio/alarms/Bright_morning.ogg:system/media/audio/alarms/Bright_morning.ogg \
	vendor/deluxe/prebuilt/common/media/audio/alarms/Cuckoo_clock.ogg:system/media/audio/alarms/Cuckoo_clock.ogg \
	vendor/deluxe/prebuilt/common/media/audio/alarms/Early_twilight.ogg:system/media/audio/alarms/Early_twilight.ogg \
	vendor/deluxe/prebuilt/common/media/audio/alarms/Full_of_wonder.ogg:system/media/audio/alarms/Full_of_wonder.ogg \
	vendor/deluxe/prebuilt/common/media/audio/alarms/Gentle_breeze.ogg:system/media/audio/alarms/Gentle_breeze.ogg \
	vendor/deluxe/prebuilt/common/media/audio/alarms/Icicles.ogg:system/media/audio/alarms/Icicles.ogg \
	vendor/deluxe/prebuilt/common/media/audio/alarms/Jump_start.ogg:system/media/audio/alarms/Jump_start.ogg \
	vendor/deluxe/prebuilt/common/media/audio/alarms/Loose_change.ogg:system/media/audio/alarms/Loose_change.ogg \
	vendor/deluxe/prebuilt/common/media/audio/alarms/Rolling_fog.ogg:system/media/audio/alarms/Rolling_fog.ogg \
	vendor/deluxe/prebuilt/common/media/audio/alarms/Spokes.ogg:system/media/audio/alarms/Spokes.ogg \
	vendor/deluxe/prebuilt/common/media/audio/alarms/Sunshower.ogg:system/media/audio/alarms/Sunshower.ogg \
	vendor/deluxe/prebuilt/common/media/audio/notifications/Beginning.ogg:system/media/audio/notifications/Beginning.ogg \
	vendor/deluxe/prebuilt/common/media/audio/notifications/Coconuts.ogg:system/media/audio/notifications/Coconuts.ogg \
	vendor/deluxe/prebuilt/common/media/audio/notifications/Duet.ogg:system/media/audio/notifications/Duet.ogg \
	vendor/deluxe/prebuilt/common/media/audio/notifications/End_note.ogg:system/media/audio/notifications/End_note.ogg \
	vendor/deluxe/prebuilt/common/media/audio/notifications/Gentle_gong.ogg:system/media/audio/notifications/Gentle_gong.ogg \
	vendor/deluxe/prebuilt/common/media/audio/notifications/Mallet.ogg:system/media/audio/notifications/Mallet.ogg \
	vendor/deluxe/prebuilt/common/media/audio/notifications/Orders_up.ogg:system/media/audio/notifications/Orders_up.ogg \
	vendor/deluxe/prebuilt/common/media/audio/notifications/Ping.ogg:system/media/audio/notifications/Ping.ogg \
	vendor/deluxe/prebuilt/common/media/audio/notifications/Pipes.ogg:system/media/audio/notifications/Pipes.ogg \
	vendor/deluxe/prebuilt/common/media/audio/notifications/Popcorn.ogg:system/media/audio/notifications/Popcorn.ogg \
	vendor/deluxe/prebuilt/common/media/audio/notifications/Shopkeeper.ogg:system/media/audio/notifications/Shopkeeper.ogg \
	vendor/deluxe/prebuilt/common/media/audio/notifications/Sticks_and_stones.ogg:system/media/audio/notifications/Sticks_and_stones.ogg \
	vendor/deluxe/prebuilt/common/media/audio/notifications/Tuneup.ogg:system/media/audio/notifications/Tuneup.ogg \
	vendor/deluxe/prebuilt/common/media/audio/notifications/Tweeter.ogg:system/media/audio/notifications/Tweeter.ogg \
	vendor/deluxe/prebuilt/common/media/audio/notifications/Twinkle.ogg:system/media/audio/notifications/Twinkle.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ringtones/Copycat.ogg:system/media/audio/ringtones/Copycat.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ringtones/Crackle.ogg:system/media/audio/ringtones/Crackle.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ringtones/Flutterby.ogg:system/media/audio/ringtones/Flutterby.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ringtones/Hotline.ogg:system/media/audio/ringtones/Hotline.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ringtones/Leaps_and_bounds.ogg:system/media/audio/ringtones/Leaps_and_bounds.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ringtones/Lollipop.ogg:system/media/audio/ringtones/Lollipop.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ringtones/Lost_and_found.ogg:system/media/audio/ringtones/Lost_and_found.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ringtones/Mash_up.ogg:system/media/audio/ringtones/Mash_up.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ringtones/Monkey_around.ogg:system/media/audio/ringtones/Monkey_around.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ringtones/Schools_out.ogg:system/media/audio/ringtones/Schools_out.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ringtones/The_big_adventure.ogg:system/media/audio/ringtones/The_big_adventure.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ringtones/Zen_too.ogg:system/media/audio/ringtones/Zen_too.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/audio_end.ogg:system/media/audio/ui/audio_end.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/audio_initiate.ogg:system/media/audio/ui/audio_initiate.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/camera_click.ogg:system/media/audio/ui/camera_click.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/camera_focus.ogg:system/media/audio/ui/camera_focus.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/Dock.ogg:system/media/audio/ui/Dock.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/Effect_Tick.ogg:system/media/audio/ui/Effect_Tick.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/InCallNotification.ogg:system/media/audio/ui/InCallNotification.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/KeypressDelete.ogg:system/media/audio/ui/KeypressDelete.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/KeypressInvalid.ogg:system/media/audio/ui/KeypressInvalid.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/KeypressReturn.ogg:system/media/audio/ui/KeypressReturn.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/KeypressSpacebar.ogg:system/media/audio/ui/KeypressSpacebar.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/KeypressStandard.ogg:system/media/audio/ui/KeypressStandard.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/Lock.ogg:system/media/audio/ui/Lock.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/LowBattery.ogg:system/media/audio/ui/LowBattery.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/NFCFailure.ogg:system/media/audio/ui/NFCFailure.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/NFCInitiated.ogg:system/media/audio/ui/NFCInitiated.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/NFCSuccess.ogg:system/media/audio/ui/NFCSuccess.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/NFCTransferComplete.ogg:system/media/audio/ui/NFCTransferComplete.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/NFCTransferInitiated.ogg:system/media/audio/ui/NFCTransferInitiated.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/Trusted.ogg:system/media/audio/ui/Trusted.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/Undock.ogg:system/media/audio/ui/Undock.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/Unlock.ogg:system/media/audio/ui/Unlock.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/VideoRecord.ogg:system/media/audio/ui/VideoRecord.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/VideoStop.ogg:system/media/audio/ui/VideoStop.ogg \
	vendor/deluxe/prebuilt/common/media/audio/ui/WirelessChargingStarted.ogg:system/media/audio/ui/WirelessChargingStarted.ogg

# Change default sounds
PRODUCT_PROPERTY_OVERRIDES += \
	ro.config.ringtone=The_big_adventure.ogg \
	ro.config.notification_sound=Popcorn.ogg \
	ro.config.alarm_alert=Bright_morning.ogg
