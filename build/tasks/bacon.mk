# Copyright (C) 2017 Unlegacy-Android
# Copyright (C) 2017 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# -----------------------------------------------------------------
# StatiX OTA update package

STATIX_TARGET_PACKAGE := $(PRODUCT_OUT)/$(STATIX_VERSION).zip

.PHONY: bacon
bacon: $(INTERNAL_OTA_PACKAGE_TARGET)
	$(hide) ln -f $(INTERNAL_OTA_PACKAGE_TARGET) $(DELUXE_TARGET_PACKAGE)
#	$(hide) $(MD5SUM) $(DELUXE_TARGET_PACKAGE) | sed "s|$(PRODUCT_OUT)/||" > $(DELUXE_TARGET_PACKAGE).md5sum
	@echo " "
	@echo " "
	@echo " "
	@echo "  ____            ___                          _____   ____         "
        @echo " /\  _`\         /\_ \                        /\  __`\/\  _`\      "
        @echo " \ \ \/\ \     __\//\ \    __  __  __  _    __\ \ \/\ \ \,\L\_\    "
        @echo "  \ \ \ \ \  /'__`\\ \ \  /\ \/\ \/\ \/'\ /'__`\ \ \ \ \/_\__ \    "
        @echo "   \ \ \_\ \/\  __/ \_\ \_\ \ \_\ \/>  <//\  __/\ \ \_\ \/\ \L\ \  " 
        @echo "    \ \____/\ \____\/\____\\ \____//\_/\_\ \____\\ \_____\ `\____\ "
        @echo "     \/___/  \/____/\/____/ \/___/ \//\/_/\/____/ \/_____/\/_____/ "
	@echo " "
	@echo " "
	@echo "Package Complete: $(DELUXE_TARGET_PACKAGE)" >&2
	@echo "Package size: `du -h $(DELUXE_TARGET_PACKAGE) | cut -f 1`"
