# Build fingerprint
ifneq ($(BUILD_FINGERPRINT),)
ADDITIONAL_BUILD_PROPERTIES += \
    ro.build.fingerprint=$(BUILD_FINGERPRINT)
endif

ADDITIONAL_BUILD_PROPERTIES += \
    ro.deluxe.version=$(DELUXE_BASE_VERSION)-$(DELUXE_BUILD_TYPE)-$(BUILD_DATE)-$(BUILD_TIME) \
    ro.deluxe.base.version=$(DELUXE_BASE_VERSION) \
    ro.mod.version=$(BUILD_ID)-$(BUILD_DATE)-$(DELUXE_BASE_VERSION) \
    ro.deluxe.fingerprint=$(ROM_FINGERPRINT) \
    ro.deluxe.buildtype=$(DELUXE_BUILD_TYPE) \
    ro.deluxe.device=$(TARGET_PRODUCT)
