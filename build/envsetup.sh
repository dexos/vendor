function __print_extra_functions_help() {
cat <<EOF
Additional functions:
- repopick:        Utility to fetch changes from Gerrit.
EOF
}

function repopick() {
    set_stuff_for_environment
    T=$(gettop)
    $T/vendor/deluxe/build/tools/repopick.py $@
}
